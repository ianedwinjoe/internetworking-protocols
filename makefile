all: server client

client:  client.o
	gcc -pthread -o client client.o
client.o: client.c
	gcc -c -Wall client.c

server: server.o
	gcc -pthread -o server server.o 
server.o:server.c
	gcc -c -Wall server.c
clean:
	rm server client server.o client.o
