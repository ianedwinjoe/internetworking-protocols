/*
   Ian Percy, Edwin Ortega, and Joseph Sands
   CS494P Winter 2018, Programming project P2
*/


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/* Client class
-  two sockets ,sock for sending recvFrom for receiving
-  still need thread for the sending? - probably smarter to do it for the sending process
*/
typedef enum{
	CONNECT,
	REQUEST,
	GETFILE,
	DISCONNECT,
	CLOSED
} state;


void appendBuffer(char,int,int,char*);  // Fills buffer with arg values
void error(const char *);               // Prints a supplied error message to the console and exits

/* Global Variables */
struct timeval tv; 
state check;	               // Allows for retransmission timeouts to be tracked
int i, sock, recvSock, n,temp;         
int seqNum, file_size; 
int recseqNum=0;               // Sequence number for received packet
int off,size,tempSize =0;      // Used to manage buffer segments 
FILE *outfile = NULL;          // Pointer to the output file
const int data_off = sizeof(char) + sizeof(int)*2; // beginning of data segment
unsigned int length;
struct sockaddr_in server,server2,from; 
struct hostent *hp;
char buffer[sizeof(char)+sizeof(int)*2+sizeof(char)*1024];
char packID,recpackID;
char fileAddr[35];
int acked[10];
int windowSize,lowestAck,diff;
int port, port2;
/* ---------------- */

int main(int argc, char *argv[])
{	
    check=CONNECT;
    windowSize=5;
    if (argc != 4){ 
        printf("Usage: IP Port FilePath\n");
        exit(1);
    }
    sock= socket(AF_INET, SOCK_DGRAM, 0);
    recvSock=socket(AF_INET,SOCK_DGRAM,0);
   
    if (sock < 0 || recvSock <0) error("socket");
    port = atoi(argv[2]);
    port2= port+1;

    server.sin_family = AF_INET;
    server2.sin_family=AF_INET;
    hp = gethostbyname(argv[1]);
    if (hp==0){ error("Unknown host"); }

    bcopy( (char *)hp->h_addr, 
           (char *)&server.sin_addr,
           hp->h_length);
	   bcopy((char *)hp->h_addr, 
           (char *)&server2.sin_addr,
           hp->h_length
         );

    server.sin_port = htons(port);
    server2.sin_port=htons(port2); // setup the second port address
    length=sizeof(struct sockaddr_in);
	

    //connect to server     
    memcpy(fileAddr,argv[3],strlen(argv[3]));  
    bzero(buffer,sizeof(buffer));
    while(check!=CLOSED){
        switch(check){
	    case CONNECT:
	        //create first packet to send to server- connection request
                packID='C';
                seqNum=0;       
                appendBuffer(packID,seqNum,0,buffer);  
                n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr*) &server,length);     // send first port
                if(n<0){ error("Send to: connection failure"); }
                n=sendto(recvSock,buffer,sizeof(buffer),0,(const struct sockaddr*)&server2,length); // send second port
                bzero(buffer, sizeof(buffer));
                n = recvfrom(recvSock,buffer,sizeof(buffer),0,(struct sockaddr *)&from, &length);
                if (n < 0){ error("recvfrom"); }
                memcpy(&recpackID,buffer,sizeof(recpackID));                                        // read response pack ID
                off=sizeof(recpackID);
                memcpy(&recseqNum,buffer+off,sizeof(recseqNum));                                    // read response pack seq number
                temp=ntohl(recseqNum);

                // connection accepted - send R packet
                if(temp==seqNum&& recpackID=='A'){ // if seq number matches and A
                    check=REQUEST;
                }
                else{
                    check=DISCONNECT;
                    break;
                }
            case REQUEST:
                write(1,"Connection to server accepted. Requesting file\n",47);
                bzero(buffer,sizeof(buffer));
                packID='R'; // now creating packet for file request
                seqNum++;
                memcpy(buffer,&packID,sizeof(packID));     // write packID to buffer
                off=sizeof(packID);
                temp=htonl(seqNum);
                memcpy(buffer+off, &temp,sizeof(seqNum));  // write seqNum to buffer
                off= off+sizeof(seqNum);
                size = strlen(fileAddr);
                tempSize=htonl(size); 
                memcpy(buffer+off,&tempSize,sizeof(size)); // write str length size
                off=off+sizeof(size);
                memcpy(buffer+off,&fileAddr,size);         // write file string addr
                n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr*) &server,length); // send to server
                if(n<0){ error("sendto: request file"); }
                if(recvfrom(recvSock,buffer,sizeof(buffer),0,(struct sockaddr *)&from, &length)<0){
                    n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr*)&server,length);
                    if(n<0){ error("sendto: request file"); }
                }   
				
				
                memcpy(&recpackID,buffer,sizeof(recpackID));
                off=sizeof(recpackID);
                memcpy(&recseqNum,buffer+off, sizeof(recseqNum));
                temp=ntohl(recseqNum);
                off+=sizeof(recseqNum);
                memcpy(&file_size, buffer+off, sizeof(file_size));
                printf("client file size: %d\n",file_size);
			  
                // Response received - check if it is an 'A' for file found, otherwise 
                if(recpackID=='A' && temp==seqNum){
                    // Send an ACK to the server so it can begin transferring the file over
                    // Currently the sequence number is not being incremented, but it might have to be
                    check=GETFILE;
                }
                else{
                    check=DISCONNECT;
                    break;
                }
            case GETFILE:  // state to read file from server 
                write(1,"give me that file baby\n",23);
                bzero(buffer, sizeof(buffer));
                packID = 'A';
                memcpy(buffer, &packID, sizeof(packID));
                off = sizeof(packID);
                memcpy(buffer+off, &recseqNum, sizeof(recseqNum));
                n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr*) &server,length);
                if(n < 0){ error("sendto failure"); }  
                else{
                    if(recpackID=='T'){
                        bzero(buffer,sizeof(buffer));
                        packID='A'; 
                        memcpy(buffer,&packID,sizeof(packID));
                        off=sizeof(packID);
                        memcpy(buffer,&recseqNum,sizeof(recseqNum)); 
                        n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr *) &server,length); 
                        if(n<0){ error("send to: close after file failure"); }
                        write(1,"closing socket client\n",22);
                        close(sock);
                        close(recvSock);
                        return 0; 
                    }
                }  

         	// At this point we are expecting the first Data ('D') packet
		// to arrive from the server 
		bzero(buffer, sizeof(buffer));
		n = recvfrom(recvSock,buffer,sizeof(buffer),0,(struct sockaddr *)&from, &length);
		if(n<0){
			error("recvfrom failure");
		}
		memcpy(&packID, buffer, sizeof(packID));
		off = sizeof(packID);
		memcpy(&recseqNum,buffer+off, sizeof(recseqNum));
		temp=ntohl(recseqNum);
		off+=sizeof(recseqNum);
		memcpy(&size,buffer+off,sizeof(size));
		tempSize=ntohl(size);
		lowestAck=recseqNum; // set lowestAck to the most recent ack received 
		for(i=0;i<10;i++){
                    acked[i]=0;
		}

		// Check if the received packet is a data ('D') packet
		if(packID == 'D'){
                    outfile = fopen("output", "wb");            // Data will be written to "output"
                    fwrite(buffer+data_off,1,tempSize,outfile); // Writes from buffer, to outfile, starting at the data segment
                    while(1){
                        bzero(buffer, sizeof(buffer));
 
                        // Send an ACK to the server
                        packID = 'A';
                        memcpy(buffer, &packID, sizeof(packID));
                        off = sizeof(packID);
                        memcpy(buffer+off, &recseqNum,sizeof(recseqNum));
                        off += sizeof(recseqNum);
                        n=sendto(sock,buffer,sizeof(buffer),0,(const struct sockaddr *) &server,length);
                        if(n < 0){ error("send to didn't work"); }
		
                        bzero(buffer, sizeof(buffer));
                        n = recvfrom(recvSock,buffer,sizeof(buffer),0,(struct sockaddr *)&from, &length);
                        if(n<0){ error("recvfrom failure"); }
                        memcpy(&packID, buffer, sizeof(packID));
                        off = sizeof(packID);
                        memcpy(&recseqNum,buffer+off, sizeof(recseqNum));
                        temp=ntohl(recseqNum);
                        off+=sizeof(recseqNum);
                        memcpy(&size,buffer+off,sizeof(size));
                        tempSize=ntohl(size);
                        if(temp==lowestAck){ // if lowestAck - adjust window
                            for(i=0;i<windowSize;i++){
                                if(acked[i]==0){
                                    break;
                                }
                            }
                            lowestAck=i+lowestAck+1;
			    diff=windowSize-i;
			    for(i=0;i<windowSize;i++){ // shift values in array
			        if(i>(windowSize-diff))
			            acked[i]=0;
			        else{
			            acked[i]=acked[diff];
                                }
                            }
		        }
                        if(packID == 'T'){
                            check=DISCONNECT; // change state to disconnect if T packet found
			    break;
                        }
                        fwrite(buffer+data_off,1,tempSize,outfile); // Writes from buffer, to outfile, starting at the data segment 
                    }
                }
                else{
		    //something is out of order, terminate maybe?
                }            
		
                break;
            case DISCONNECT:
                fclose(outfile); // needed or else file will not write 
                if(sock) { // close both sockets
                    close(sock);
                    if(recvSock) {
                        close(sock);
                        check=CLOSED;
                        break;		
                    }	   
	        }
        }
    }
    return 0;
}

void appendBuffer(char packID, int seq, int size, char *buffer){
    int temp,off=0;
    bzero(buffer,sizeof(&buffer));
    memcpy(buffer,&packID,sizeof(packID));
    off=sizeof(packID);
    temp=ntohl(seq);
    memcpy((buffer+off),&temp,sizeof(seq));
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}
